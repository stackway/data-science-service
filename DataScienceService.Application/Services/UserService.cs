using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Raven.Client.Documents;
using DataScienceService.Domain.Entities;
using DataScienceService.Application.Exceptions;

namespace DataScienceService.Application.Services {
    public interface IUserService {
        User GetUser(long persistenceId);

        void AddUser(User user);
        void AddUser(User user, ViewedResource resource);

        void UpdateUser(User user);

        void RemoveUser(long persistenceId);
    }

    public class UserService : IUserService {
        private readonly IDocumentStore _store;
        private readonly ILogger _logger;

        public UserService(
            IDocumentStore store,
            ILogger<UserService> logger
        ) {
            _store = store;
            _logger = logger;
        }

        public User GetUser(long persistenceId) {
            User foundUser;

            using (var session = _store.OpenSession()) {
                foundUser = session.Query<User>()
                    .Where(u => u.PersistanceId == persistenceId)
                    .FirstOrDefault();
            }

            if (foundUser == null) {
                throw new EntityNotFoundException();
            }

            return foundUser;
        }

        public void AddUser(User user) {
            _logger.LogInformation(
                "try to insert user to db: {email}",
                user.Email
            );

            using (var session = _store.OpenSession()) {
                session.Store(user);
                session.SaveChanges();
            }

            _logger.LogInformation(
                "user inserted to db: {email}",
                user.Email
            );
        }

        public void AddUser(User user, ViewedResource resource) {
            AddUser(user.Actions()
                .PushViewedResource(resource)
                .DequeueViewedResource(r => r.Count() > 50)
                .User()
            );
        }

        public void UpdateUser(User user) {
            using (var session = _store.OpenSession()) {
                session.Store(user, user.Id);
                session.SaveChanges();
            }
        }

        public void RemoveUser(long persistenceId) {
            User user;

            try {
                user = GetUser(persistenceId);
            }
            catch (EntityNotFoundException) {
                _logger.LogWarning(
                    "user not found by persistence id: {id}",
                    persistenceId
                );

                return;
            }

            using (var session = _store.OpenSession()) {
                session.Delete(user);
                session.SaveChanges();
            }

            _logger.LogInformation(
                "user was removed by persistence id: {id}",
                persistenceId
            );
        }
    }
}