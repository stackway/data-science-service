using System.Linq;
using System.Collections.Generic;

using Raven.Client.Documents;

using DataScienceService.Domain.Entities;
using DataScienceService.Domain.Units;
using DataScienceService.Application.Exceptions;

namespace DataScienceService.Application.Services {
    public class SimilarUsersService : IHandleToFindSimilarUsers {
        private readonly IDocumentStore _store;
        private readonly IUserService _service;

        public SimilarUsersService(
            IDocumentStore store,
            IUserService service
        ) {
            _store = store;
            _service = service;
        }

        public IEnumerable<User> FindSimilarUsers(User user, int selectCount) {
            User foundUser;

            try {
                foundUser = _service.GetUser(user.PersistanceId);
            }
            catch (EntityNotFoundException) {
                return new List<User>();
            }

            IEnumerable<User> users;

            using (var session = _store.OpenSession()) {
                users = session.Query<User>().ToList();
            }

            return users.Select(
                    u => new { Distance = user.EuclideanDistance(u), User = u }
                )
                .OrderBy(matched => matched.Distance)
                .Take(selectCount)
                .Where(u => u.User.PersistanceId != user.PersistanceId)
                .Select(matched => matched.User);
        }
    }
}