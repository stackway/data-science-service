using System.Linq;
using System.Threading.Tasks;

using Microsoft.Extensions.Logging;

using Raven.Client.Documents;

using DataScienceService.Domain.Entities;
using DataScienceService.Domain.Units;

namespace DataScienceService.Application.Services {
    public class UserTagsViewsService : IHandleToUpdateUserTagsViewsUnit {
        private readonly IDocumentStore _store;
        private readonly IUserService _service;
        private readonly ILogger _logger;

        public UserTagsViewsService(
            IDocumentStore store,
            IUserService service,
            ILogger<UserTagsViewsService> logger
        ) {
            _store = store;
            _service = service;
            _logger = logger;
        }

        public void UpdateUserTagsViews(User user, ViewedResource resource) {
            User foundUser;

            using (var session = _store.OpenSession()) {
                foundUser = session.Query<User>()
                    .Where(u => u.PersistanceId == user.PersistanceId)
                    .FirstOrDefault();
            }

            if (foundUser == null) {
                _logger.LogInformation(
                    "try to add new user: {user}",
                    user
                );

                _service.AddUser(user, resource);

                return;
            }

            _logger.LogInformation(
                "try to update user tags: {email}",
                user.Email
            );

            var updatedUser = foundUser.Actions()
                .PushViewedResource(resource)
                .DequeueViewedResource(r => r.Count() > 50)
                .User();

            _service.UpdateUser(updatedUser);
        }
    }
}