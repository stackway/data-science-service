using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using AutoMapper;
using DataScienceService.Domain.Entities;
using DataScienceService.Domain.Units;
using DataScienceService.Infrastructure.RabbitMQ;
using DataScienceService.Infrastructure.Persistance;
using DataScienceService.Application.Services;
using DataScienceService.Interface.MessageBus;

namespace DataScienceService.Interface {
    public class Startup {
        public Startup(IConfiguration configuration) {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services) {
            services.AddHealthChecks();

            services
                .AddMvc()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            services.AddSingleton<IConfiguration>(Configuration);

            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

            services.ConfigurePersistance(Configuration);

            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IHandleToUpdateUserTagsViewsUnit, UserTagsViewsService>();
            services.AddScoped<IHandleToFindSimilarUsers, SimilarUsersService>();

            services.ConfigureRabbitMqConnection(Configuration);

            services
                .ConfigureMqRoutes()
                .ConfigureMqServices()
                .ConfigureMqSenders()
                .ConfigureMqConsumers()
                .ConfigureMqHostedServices();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env) {
            if (env.IsDevelopment()) app.UseDeveloperExceptionPage();
            else app.UseHsts();

            app.UseHealthChecks(
                "/health",
                new HealthCheckOptions { Predicate = check => check.Tags.Contains("ready") }
            );

            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}