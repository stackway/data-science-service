using System.Linq;
using System.Collections.Generic;
using AutoMapper;
using DataScienceService.Domain.Entities;
using DataScienceService.Infrastructure.RabbitMQ;

namespace DataScienceService.Interface.MessageBus {
    public interface UpdatedSimilarUsersMessage {
        long Id { get; }
        string Email { get; }

        IEnumerable<long> SimilarUsers { get; }
    }

    public class UpdatedBaseSimilarUsersMessage : UpdatedSimilarUsersMessage {
        public long Id { get; set; }
        public string Email { get; set; }

        public IEnumerable<long> SimilarUsers { get; set; }

        public UpdatedBaseSimilarUsersMessage UseSimilarUsers(
            IEnumerable<long> similarUsers
        ) {
            SimilarUsers = similarUsers;

            return this;
        }
    }

    public class UpdatedSimilarUsersMessageProfile : Profile {
        public UpdatedSimilarUsersMessageProfile() {
            CreateMap<User, UpdatedBaseSimilarUsersMessage>()
                .ForMember(
                    d => d.Id,
                    opt => opt.MapFrom(o => o.PersistanceId)
                );
        }
    }

    public class UpdatedSimilarUsersMqRoute : RabbitMqSenderRoute<UpdatedSimilarUsersMessage> {
        public UpdatedSimilarUsersMqRoute() {
            Exchange = "updated-similar-users";
        }
    }

    public class UpdatedSimilarUsersEmitter {
        private readonly IRabbitMqSender<UpdatedSimilarUsersMessage> _sender;
        private readonly IMapper _mapper;

        public UpdatedSimilarUsersEmitter(
            IRabbitMqSender<UpdatedSimilarUsersMessage> sender,
            IMapper mapper
        ) {
            _sender = sender;
            _mapper = mapper;
        }

        public void Publish(UpdatedSimilarUsersMessage contract) {
            _sender.Send(contract);
        }

        public void Publish(User user, IEnumerable<User> similarUsers) {
            Publish(
                _mapper.Map<User, UpdatedBaseSimilarUsersMessage>(user)
                    .UseSimilarUsers(similarUsers.Select(u => u.PersistanceId))
            );
        }
    }
}