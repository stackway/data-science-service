using DataScienceService.Infrastructure.RabbitMQ;
using Microsoft.Extensions.DependencyInjection;

namespace DataScienceService.Interface.MessageBus {
    public static class MessageBusContext {
        public static IServiceCollection ConfigureMqRoutes(this IServiceCollection services) {
            services.AddSingleton<
                IRabbitMqSenderRoute<UpdatedSimilarUsersMessage>,
                UpdatedSimilarUsersMqRoute
            >();

            services
                .AddSingleton<IRabbitMqConsumerRoute<UsedResourceMessage>, UsedResourceMqRoute>()
                .AddSingleton<IRabbitMqConsumerRoute<UpdateSimilarUsersMessage>, UpdateSimilarUsersMqRoute>()
                .AddSingleton<IRabbitMqConsumerRoute<DeleteUserMessage>, DeleteUserMqRoute>();

            return services;
        }

        public static IServiceCollection ConfigureMqServices(this IServiceCollection services) {
            services
                .AddScoped<IRabbitMqConsumerService<UsedResourceMessage>, UsedResourceConsumer>()
                .AddScoped<IRabbitMqConsumerService<UpdateSimilarUsersMessage>, UpdateSimilarUsersConsumer>()
                .AddScoped<IRabbitMqConsumerService<DeleteUserMessage>, DeleteUserConsumerService>();

            return services;
        }

        public static IServiceCollection ConfigureMqSenders(this IServiceCollection services) {
            services.AddSingleton<
                IRabbitMqSender<UpdatedSimilarUsersMessage>,
                RabbitMqSender<UpdatedSimilarUsersMessage>
            >();

            return services.AddScoped<UpdatedSimilarUsersEmitter>();
        }

        public static IServiceCollection ConfigureMqConsumers(this IServiceCollection services) {
            services
                .AddScoped<
                    IRabbitMqConsumer<UsedResourceMessage>,
                    RabbitMqConsumer<UsedResourceMessage>
                >()
                .AddScoped<
                    IRabbitMqConsumer<UpdateSimilarUsersMessage>,
                    RabbitMqConsumer<UpdateSimilarUsersMessage>
                >()
                .AddScoped<
                    IRabbitMqConsumer<DeleteUserMessage>,
                    RabbitMqConsumer<DeleteUserMessage>
                >();

            return services;
        }

        public static IServiceCollection ConfigureMqHostedServices(this IServiceCollection services) {
            services
                .AddHostedService<RabbitMqConsumerHostedService<UsedResourceMessage>>()
                .AddHostedService<RabbitMqConsumerHostedService<UpdateSimilarUsersMessage>>()
                .AddHostedService<RabbitMqConsumerHostedService<DeleteUserMessage>>();
            
            return services;
        }
    }
}