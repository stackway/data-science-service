using System.Linq;
using AutoMapper;
using DataScienceService.Domain.Units;
using DataScienceService.Domain.Entities;
using DataScienceService.Infrastructure.RabbitMQ;
using Microsoft.Extensions.Logging;

namespace DataScienceService.Interface.MessageBus {
    public class UpdateSimilarUsersMessage {
        public long Id { get; set; }
        public string Email { get; set; }

        public int CountToCollect { get; set; }
    }

    public class SimilarUsersMessageProfile : Profile {
        public SimilarUsersMessageProfile() {
            CreateMap<UpdateSimilarUsersMessage, User>()
                .ForMember(
                    d => d.PersistanceId,
                    opt => opt.MapFrom(o => o.Id)
                )
                .ConstructUsing(u => new User());
        }
    }

    public class UpdateSimilarUsersMqRoute : RabbitMqConsumerRoute<UpdateSimilarUsersMessage> {
        public UpdateSimilarUsersMqRoute() {
            Queue = "data-science-service.update-similar-users";
            Exchange = "update-similar-users";
        }
    }

    public class UpdateSimilarUsersConsumer : IRabbitMqConsumerService<UpdateSimilarUsersMessage> {
        private readonly IHandleToFindSimilarUsers _handler;
        private readonly IMapper _mapper;
        private readonly ILogger _logger;

        private readonly UpdatedSimilarUsersEmitter _emitter;

        public UpdateSimilarUsersConsumer(
            IHandleToFindSimilarUsers handler,
            UpdatedSimilarUsersEmitter emitter,
            IMapper mapper,
            ILogger<UpdateSimilarUsersConsumer> logger
        ) {
            _handler = handler;
            _emitter = emitter;
            _mapper = mapper;
            _logger = logger;
        }

        public void ProcessMessage(UpdateSimilarUsersMessage message) {
            var user = _mapper.Map<UpdateSimilarUsersMessage, User>(message);

            var similarUsers = _handler
                .FindSimilarUsers(
                    user,
                    message.CountToCollect
                );

            if (!similarUsers.Any()) {
                _logger.LogWarning(
                    "can't find similar users for: {email}",
                    user.Email
                );

                return;
            }

            _emitter.Publish(
                user,
                similarUsers
            );
        }
    }
}