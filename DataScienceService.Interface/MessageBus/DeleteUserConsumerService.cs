using DataScienceService.Application.Services;
using DataScienceService.Infrastructure.RabbitMQ;
using Microsoft.Extensions.Logging;

namespace DataScienceService.Interface.MessageBus {
    public class DeleteUserMessage {
        public long Id { get; set; }
        public string Email { get; set; }
    }

    public class DeleteUserMqRoute : RabbitMqConsumerRoute<DeleteUserMessage> {
        public DeleteUserMqRoute() {
            Queue = "data-science-service.delete-user";
            Exchange = "delete-user";
            ExchangeType = RabbitMqExchangeType.Fanout;
        }
    }

    public class DeleteUserConsumerService : IRabbitMqConsumerService<DeleteUserMessage> {
        private readonly IUserService _service;
        private readonly ILogger _logger;

        public DeleteUserConsumerService(
            IUserService service,
            ILogger<DeleteUserConsumerService> logger
        ) {
            _service = service;
            _logger = logger;
        }

        public void ProcessMessage(DeleteUserMessage message) {
            _service.RemoveUser(message.Id);
        }
    }
}