using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using DataScienceService.Domain.Entities;
using DataScienceService.Domain.Units;
using DataScienceService.Infrastructure.RabbitMQ;
using Microsoft.Extensions.Logging;

namespace DataScienceService.Interface.MessageBus {
    public class UsedResourceMessage {
        public long UserId { get; set; }
        public string UserEmail { get; set; }

        public string Name { get; set; }
        public DateTime ViewedAt { get; set; }

        public IEnumerable<string> Tags { get; set; }

        public override string ToString() {
            if (UserEmail != null) {
                return UserEmail;
            }

            return "[message is broken]";
        }
    }

    public class UsedResourceMessageProfile : Profile {
        public UsedResourceMessageProfile() {
            CreateMap<UsedResourceMessage, User>()
                .ForMember(
                    d => d.PersistanceId,
                    opt => opt.MapFrom(o => o.UserId)
                )
                .ForMember(
                    d => d.Email,
                    opt => opt.MapFrom(o => o.UserEmail)
                )
                .ConstructUsing(u => new User());

            CreateMap<UsedResourceMessage, ViewedResource>()
                .ConstructUsing(v => new ViewedResource());
        }
    }

    public class UsedResourceMqRoute : RabbitMqConsumerRoute<UsedResourceMessage> {
        public UsedResourceMqRoute() {
            Queue = "data-science-service.used-resource";
            Exchange = "used-resource";
        }
    }

    public class UsedResourceConsumer : IRabbitMqConsumerService<UsedResourceMessage> {
        private readonly IHandleToUpdateUserTagsViewsUnit _handler;
        private readonly IMapper _mapper;
        private readonly ILogger _logger;

        public UsedResourceConsumer(
            IHandleToUpdateUserTagsViewsUnit handler,
            IMapper mapper,
            ILogger<UsedResourceConsumer> logger
        ) {
            _handler = handler;
            _mapper = mapper;
            _logger = logger;
        }

        public void ProcessMessage(UsedResourceMessage message) {
            _logger.LogInformation(
                "try to map the message: {email}",
                message
            );

            var user = _mapper.Map<UsedResourceMessage, User>(message);
            var viewedResource = _mapper.Map<UsedResourceMessage, ViewedResource>(message);

            _logger.LogInformation(
                "resource parsed for: {email} at {time}",
                user.Email,
                viewedResource.ViewedAt
            );

            _handler.UpdateUserTagsViews(
                user,
                viewedResource
            );
        }
    }
}