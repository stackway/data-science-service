using System.Threading.Tasks;

using DataScienceService.Domain.Entities;

namespace DataScienceService.Domain.Units {
    public interface IHandleToUpdateUserTagsViewsUnit {
        void UpdateUserTagsViews(User user, ViewedResource resource);
    }
}