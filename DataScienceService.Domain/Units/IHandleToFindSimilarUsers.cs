using System.Collections.Generic;
using System.Threading.Tasks;

using DataScienceService.Domain.Entities;

namespace DataScienceService.Domain.Units {
    public interface IHandleToFindSimilarUsers {
        /// <summary>
        /// find similar users by some vector
        /// </summary>
        /// <param name="selectCount">max count of similar users to select</param>
        /// <returns></returns>
        IEnumerable<User> FindSimilarUsers(User user, int selectCount);
    }
}