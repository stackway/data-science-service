using System;
using System.Collections.Generic;

using DataScienceService.Domain.Actions;

namespace DataScienceService.Domain.Entities {
    public class User {
        public string Id { get; set; }

        public long PersistanceId { get; set; }

        public string Email { get; set; }

        public IDictionary<string, int> TagsViews { get; set; } = new Dictionary<string, int>();
        public IList<ViewedResource> LastViewedResources { get; set; } = new List<ViewedResource>();

        public User() { }
        public User(long persistanceId, string email) {
            PersistanceId = persistanceId;
            Email = email;
        }

        public TagsActions Actions() => new TagsActions(this);

        public int GetTagViews(string tag) {
            return TagsViews.ContainsKey(tag) ? TagsViews[tag] : 0;
        }

        public double EuclideanDistance(User another) {
            var distance = 0d;

            foreach (var tagViews in TagsViews) {
                distance += Math.Pow(
                    tagViews.Value - another.GetTagViews(tagViews.Key),
                    2
                );
            }

            return distance;
        }
    }
}