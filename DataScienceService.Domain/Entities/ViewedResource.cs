using System;
using System.Collections.Generic;
using System.Linq;

namespace DataScienceService.Domain.Entities {
    public class ViewedResource {
        public string Name { get; set; }
        public DateTime ViewedAt { get; set; }

        public IEnumerable<string> Tags { get; set; } = new List<string>();

        public ViewedResource() { }
        public ViewedResource(
            string name,
            DateTime viewedAt,
            IEnumerable<string> tags
        ) {
            Name = name;
            ViewedAt = viewedAt;
            Tags = tags;
        }
    }
}