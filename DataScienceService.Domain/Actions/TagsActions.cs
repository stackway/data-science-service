using System;
using System.Collections.Generic;
using System.Linq;

using DataScienceService.Domain.Entities;

namespace DataScienceService.Domain.Actions {
    public class TagsActions {
        private readonly User _user;

        public TagsActions(User user) {
            _user = user;
        }

        public TagsActions PushViewedResource(ViewedResource resource) {
            _user.LastViewedResources.Add(resource);

            foreach (var tag in resource.Tags) {
                var views = 1;

                if (_user.TagsViews.ContainsKey(tag))
                    views += _user.TagsViews[tag];

                _user.TagsViews[tag] = views;
            }

            return this;
        }

        public TagsActions PopViewedResource() {
            if (!_user.LastViewedResources.Any())
                return this;

            var lastViewedResources = _user.LastViewedResources.First();

            foreach (var tag in lastViewedResources.Tags) {
                var views = -1;

                if (_user.TagsViews.ContainsKey(tag))
                    views += _user.TagsViews[tag];

                _user.TagsViews[tag] = views >= 0 ? views : 0;
            }

            return this;
        }

        public TagsActions DequeueViewedResource(
            Func<IEnumerable<ViewedResource>, bool> predicate
        ) {
            if (predicate(_user.LastViewedResources))
                PopViewedResource();

            return this;
        }

        public User User() {
            return _user;
        }
    }
}