namespace DataScienceService.Infrastructure.RabbitMQ {
    public interface IRabbitMqConsumerService<TM> {
        void ProcessMessage(TM message);
    }
}