using System;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace DataScienceService.Infrastructure.RabbitMQ {
    public interface IRabbitMqConsumer<TM> : IDisposable {
        void SetupConsumer();
        Task SetupConsumerByStatus();
    }

    public class RabbitMqConsumer<TM> : IRabbitMqConsumer<TM> {
        private readonly IRabbitMqConsumerRoute<TM> _route;
        private readonly IModel _channel;
        private readonly IRabbitMqConsumerService<TM> _service;
        private readonly ILogger _logger;

        public RabbitMqConsumer(
            IConnection connection,
            IRabbitMqConsumerRoute<TM> route,
            IRabbitMqConsumerService<TM> service,
            ILogger<RabbitMqConsumer<TM>> logger
        ) {
            _route = route;
            _channel = connection.CreateModel();
            _service = service;
            _logger = logger;

            SetupRabbitMqRoute();
        }

        private void SetupRabbitMqRoute() {
            _channel.QueueDeclare(
                _route.Queue,
                _route.IsDurable,
                _route.IsExclusive,
                _route.IsAutoDelete
            );

            _channel.ExchangeDeclare(
                _route.Exchange,
                RabbitMqExchangeTypeResolver.Resolve(
                    _route.ExchangeType
                )
            );

            _channel.QueueBind(
                _route.Queue,
                _route.Exchange,
                _route.RoutingKey
            );
        }

        public void SetupConsumer() {
            var consumer = new EventingBasicConsumer(_channel);

            consumer.Received += (ch, ea) => {
                try {
                    var decodedMessage = Encoding.UTF8.GetString(ea.Body);

                    _logger.LogInformation(
                        "[x] message recivied: {message}",
                        decodedMessage
                    );

                    _service.ProcessMessage(
                        JsonConvert.DeserializeObject<TM>(decodedMessage)
                    );
                }
                catch (JsonSerializationException exception) {
                    _logger.LogWarning(exception.Message);
                }
                catch (Exception exception) {
                    _logger.LogInformation(
                        "something wrong: {}",
                        exception
                    );
                }
            };

            consumer.Shutdown += OnConsumerShutdown;
            consumer.Registered += OnConsumerRegistered;
            consumer.Unregistered += OnConsumerUnregistered;
            consumer.ConsumerCancelled += OnConsumerCancelled;

            _channel.BasicConsume(
                _route.Queue,
                true,
                consumer
            );
        }

        public Task SetupConsumerByStatus() {
            SetupConsumer();

            return Task.CompletedTask;
        }

        private void OnConsumerCancelled(object sender, ConsumerEventArgs e) {
            _logger.LogInformation(
                "consumer is cancelled by route: {route}",
                _route.Queue
            );
        }

        private void OnConsumerUnregistered(object sender, ConsumerEventArgs e) {
            _logger.LogInformation(
                "consumer is unregistered by route: {route}",
                _route.Queue
            );
        }

        private void OnConsumerRegistered(object sender, ConsumerEventArgs e) {
            _logger.LogInformation(
                "consumer is registered by route: {route}",
                _route.Queue
            );
        }

        private void OnConsumerShutdown(object sender, ShutdownEventArgs e) {
            _logger.LogInformation(
                "consumer is shutdown by route: {route}",
                _route.Queue
            );
        }

        public void Dispose() {
            _channel.Dispose();
        }
    }
}