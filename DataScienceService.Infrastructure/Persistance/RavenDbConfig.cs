using Microsoft.Extensions.Configuration;

namespace DataScienceService.Infrastructure.Persistance {
    public class RavenDbConfig {
        public readonly string Database;
        public readonly string Host;
        public readonly int Port;

        public RavenDbConfig(
            string database,
            string host,
            int port
        ) {
            Database = database;
            Host = host;
            Port = port;
        }

        public static RavenDbConfig From(IConfiguration config) {
            return new RavenDbConfig(
                config["RAVEN:DB"],
                config["RAVEN:HOST"],
                config.GetValue<int>("RAVEN:PORT")
            );
        }

        public string ConnectionString {
            get {
                var connection = $@"{Host}:{Port}";
                return $@"http://{connection}";
            }
        }
    }
}