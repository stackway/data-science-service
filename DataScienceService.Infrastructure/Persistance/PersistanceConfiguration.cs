using DataScienceService.Domain.Entities;

using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using Raven.Client.Documents;
using Raven.Client.Documents.Session;

namespace DataScienceService.Infrastructure.Persistance {
    public static class PersistanceConfugration {
        public static IServiceCollection ConfigurePersistance(
            this IServiceCollection services,
            IConfiguration configuration
        ) {
            var ravenConfig = RavenDbConfig.From(configuration);

            services.AddSingleton<RavenDbConfig>(ravenConfig);

            services.AddSingleton<IDocumentStore>(
                provider => new DocumentStore() {
                    Urls = new[] { ravenConfig.ConnectionString },
                    Database = ravenConfig.Database
                }.Initialize()
            );

            services.AddScoped<IDocumentSession>(
                provider => provider
                    .GetService<IDocumentStore>()
                    .OpenSession()
            );

            services.AddScoped<IAsyncDocumentSession>(
                provider => provider
                    .GetService<IDocumentStore>()
                    .OpenAsyncSession()
            );

            return services;
        }
    }
}