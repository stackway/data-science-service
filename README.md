# Overview

    DataScience and all related things

## Resources

- <https://github.com/dotnet-architecture/eShopOnContainers/tree/master>

- <https://docs.microsoft.com/ru-ru/dotnet/standard/microservices-architecture/microservice-ddd-cqrs-patterns/>

## MassTransit

    Message Bus

- <http://masstransit-project.com/MassTransit/usage/transports.html#rabbitmq-configuration-options>

- <http://masstransit-project.com/MassTransit/usage/configuration.html#masstransit-and-aspnet-core>

- <http://masstransit-project.com/MassTransit/introduction/packages.html>

- <https://stackoverflow.com/questions/47954136/masstransit-and-net-core-di-how-to-resolve-dependencies-with-parameterless-co>

- <http://masstransit-project.com/MassTransit/usage/message-contracts.html>

## RabbitMQ

    Message Broker

- <https://docs.docker.com/samples/library/rabbitmq/>

- <https://github.com/micahhausler/rabbitmq-compose/blob/master/docker-compose.yml>

## MongoDB

    Document oriented DB - Persistance Layer

- <https://docs.microsoft.com/en-us/aspnet/core/tutorials/first-mongo-app?view=aspnetcore-2.2&tabs=visual-studio>

- <https://docs.mongodb.com/ecosystem/drivers/csharp/>

- <http://api.mongodb.com/csharp/current/html/R_Project_CSharpDriverDocs.htm>

- <https://medium.com/@kristaps.strals/docker-mongodb-net-core-a-good-time-e21f1acb4b7b>

- <https://medium.com/@kahana.hagai/docker-compose-with-node-js-and-mongodb-dbdadab5ce0a>

- <https://gist.github.com/wesleybliss/29d4cce863f5964a3eb73c42501d99e4>

- <https://docs.docker.com/samples/library/mongo/>

## Docker

    Manual

```bash
# login
docker login \
--username oauth \ # type of token used
--password <OAuth token> \
registry.gitlab.com

# build image
docker build -t registry.gitlab.com/<registry-id>/<image-id>[:version-id]

# push image
docker push registry.gitlab.com/<registry-id>/<image-id>[:version-id]
```

    Cake

```bash
dotnet cake build.cake --target=PushImage --registry=registry.gitlab.com --user=$GITLAB_USER --pass=$GITLAB_PASS
```
